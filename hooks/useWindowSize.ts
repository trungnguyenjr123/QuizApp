'use client'
import { useEffect, useState } from "react"


export default function useWindowSize() {
    const isClient = typeof window !== "undefined";

    const [windowSize, setWindowSize] = useState({
        width: isClient ? window.innerWidth : undefined,
        height: isClient ? window.innerHeight : undefined,
    })

    useEffect(() => {
        const handler = (() => {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight
            })
        })
        window.addEventListener("resize", handler)

        return () => {
            window.removeEventListener("resize", handler)
        }
    }, [])

    return windowSize
}