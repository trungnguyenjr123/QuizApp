'use client'
import Breadcrumbs from '@mui/material/Breadcrumbs';
import { usePathname } from 'next/navigation'
import Link from 'next/link'
import Typography from "@mui/material/Typography";
import { useParams } from 'next/navigation'
import { useDispatch, useSelector } from 'react-redux';
import { sendRequest } from '@/utils/api';
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';
import { QuizState, doGetQuizAction } from '@/redux/quiz/quizSlice';

const Breadcrums = () => {
    const pathname = usePathname();
    const params = useParams<{ tag: string; slug: string }>()
    const { data: session } = useSession()
    const dispatch = useDispatch()
    const quiz = useSelector((state: QuizState) => state.quiz)
    const getAllQuiz = async () => {
        if (session) {
            const res = await sendRequest<IBackendRes<IGetQuizByParticipant>>({
                url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/api/v1/quiz/all`,
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${session?.access_token}`,
                },
            })

            if (res) {
                dispatch(doGetQuizAction(res.DT))

            }

        }
    };

    useEffect(() => {
        getAllQuiz()
    }, [session])


    const getRandomNumber = (min: number, max: number) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const breadcrumbs = [
        <Link href={'/'} style={{ textDecoration: 'none' }}>
            <Typography key="1" sx={{ color: pathname === '/' ? 'var(--breadcrumb-active)' : 'var(--fg)' }} >
                Home
            </Typography>
        </Link>,
        <Link href={'/quiz'} style={{ textDecoration: 'none' }}>
            <Typography key="2" sx={{ color: pathname === '/quiz' ? 'var(--breadcrumb-active)' : 'var(--fg)' }} >
                List Quiz
            </Typography>
        </Link>,
        //@st-ignore
        <Link href={`/quiz/${getRandomNumber(1, quiz.quiz.length)}`} style={{ textDecoration: 'none' }}>
            <Typography key="3" sx={{ color: pathname === `/quiz/${params.slug}` ? `var(--breadcrumb-active)` : 'var(--fg)' }}>
                Quiz
            </Typography>
        </Link>,

    ];

    return (
        <>
            <Breadcrumbs separator="›" aria-label="breadcrumb" sx={{ color: 'var(--fg)' }}>
                {breadcrumbs}
            </Breadcrumbs>
        </>
    )
}

export default Breadcrums;