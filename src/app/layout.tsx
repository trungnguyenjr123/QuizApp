import type { Metadata } from "next";
import { Arimo } from 'next/font/google'
import "./globals.css";
import NextAuthWrapper from "@/lib/next.auth.wrapper";
import { Provider } from 'react-redux'
import { store } from '@/redux/store'
import { Providers } from "@/redux/Provider";
export const metadata: Metadata = {
  title: "QuizzApp",
  description: "QuizzApp",
  icons: {
    icon: [
      {
        media: '(prefers-color-scheme: light)',
        url: '/icons.png',
      },
      {
        media: '(prefers-color-scheme: dark)',
        url: '/icons.png',
      },
    ],
  },
  keywords: ["quiz", "quiz app", "practice", "quiz for web"]
};

const arimo = Arimo({
  subsets: ['latin'],
  display: 'swap',
})

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Providers>
      <html suppressHydrationWarning lang="en" className={arimo.className}>
        <body>
          <NextAuthWrapper>
            {children}
          </NextAuthWrapper>
        </body>
      </html>
    </Providers>
  );
}
