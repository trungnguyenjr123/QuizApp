
import { createSlice } from '@reduxjs/toolkit'

export interface QuizState {
    quiz: {
        id: any,
        description: string,
        difficulty: string,
        image: string,
        name: string,
    }[]
}

const initialState: QuizState = {
    quiz: []
}

export const quizSlice = createSlice({
    name: 'quiz',
    initialState,
    reducers: {
        doGetQuizAction: (state, action) => {
            state.quiz = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const { doGetQuizAction } = quizSlice.actions

export default quizSlice.reducer